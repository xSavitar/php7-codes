<?php
// Return type "int" is declared in functions.

declare(strict_types=1); // enforces the strict type mode

// this method returns integers only
function returnIntValue(int $value): int {
	// this will fail since strict mode is enabled but 
	// int is added to float.
	return $value + 1.0;
}

print(returnIntValue(10)); // return will failt since 1.0 is not int

?>