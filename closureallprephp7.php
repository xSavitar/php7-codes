<?php

// Closure::call() temporarily binds an object scope to a closure 
// and invoke it. It's much faster in performance compared to "bindTo" 
// method in 5.6

// declare a class of name A
class A {

	private $x = 1;

}

// Define a clusure Pre PHP 7 Code

$getValue = function() {

	return $this->x;

};

// Bing a closure
$value = $getValue->bindTo(new A, 'A');

print($value());


?>