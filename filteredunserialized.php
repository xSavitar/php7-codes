<?php

// Filtered unserialize() for better security when unserializing objects on 
// unstrusted data sources.

// Define the classes below
class FirstClass {

	public $obj1prop;

}

class SecondClass {

	public $obj2prop;

}

// create objects
$obj1 = new FirstClass();

$obj1->obj1prop = 1;

$obj2 = new SecondClass();

$obj2->obj2prop = 2;


// Serialize the data

$serializedObj1 = serialize($obj1);

$serializedObj2 = serialize($obj2);


// default behaviour that accepts all classes second argument can be ommited.
// if allowed_classes is passed as false, unserialize convert all objects into 
// __PHP_Incomplete_Class object.

$data = unserialize($serializedObj1, ["allowed_classes" => true]);

// converts all objects into __PHP_Incomplete_Class object except those of FirstClass
// and SecondClass.

$data2 = unserialize($serializedObj2, ["allowed_classes" => ["FirstClass", "SecondClass"]]);


// display the unserialized data
print("Unserialized Obj1 Prop: " . $data->obj1prop);
print("<br />");
print("Unserialized Obj2 Prop: " . $data2->obj2prop);

?>