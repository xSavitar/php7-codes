<?php

// In PHP5.6, we define a constant array using the keyword 
// "const" but now in PHP7, it's done using the define() function.

// define an array using define funciton

define('animals', [
	'dog',
	'cat',
	'bird'
]);

// one thing to notice there is that there is not "$"
// before the variable name. Is this how it's done for 
// constants?
print(animals[1]);


?>