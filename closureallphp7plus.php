<?php

// Declare a class of name A
class A {

	private $x = 1;

}

// PHP 7+ code, Define
$value = function() {

	return $this->x;

};

// bind and call.
print($value->call(new A));