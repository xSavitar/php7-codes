<?php

// New feature called the spaceship operator (<=>) which 
// compares 2 (A and B) expressions. It returns -1, 0 or 1 when
// expr A <, = or > expr B respectively.

print( 1 <=> 1 ); br();
print( 1 <=> 2 ); br();
print( 2 <=> 1 ); br();
br();

// float comparison

print( 1.5 <=> 1.5 ); br();
print( 1.5 <=> 2.5 ); br();
print( 2.5 <=> 1.5 ); br();
br();

// string comparison

print( "a" <=> "a" ); br();
print( "a" <=> "b" ); br();
print( "b" <=> "a" ); br();

// define the break method
function br(){
	print("<br />");
}

?>