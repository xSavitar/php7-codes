<?php

// Coercive mode

function sum(int ...$ints) {
	return array_sum($ints);
}

// print the sum & implicitly cast everything 
// to an integer then calls sum()
print("The sum is: " . sum(2, '3', 4.1));

?>