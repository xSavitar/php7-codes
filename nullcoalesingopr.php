<?php

// A new feature brought into PHP7 called the "Null Coalescing Operator" 
// which comes to replace the "tenary" operature and adds to it an isset() 
// test :)

// fetch the value of $_GET['user'] and returns 'not passed' if username 
// is not passed

$username = $_GET['username'] ?? 'not passed';

print($username);

print("<br />");

// Equivalent code "Line 10 - 14" using the tenary operator

$username = isset($_GET['username']) ? $_GET['username'] : 'not passed';

print($username);

print("<br />");

// Changing ?? operator

$username = $_GET['username'] ?? $_POST['username'] ?? 'not passed';

print($username)

// NOTE: Running this file only will all result to a 'not passed text on the browser'
//  	 but running it with a paramter say: "?username=Derick" all return the text 
// 		 Derick on the browser indicating it was passed :)


?>