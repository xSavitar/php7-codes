<?php

// Anonymous class experimentation

interface Logger {

	public function log(string $msg);

}

class Application {

	private $logger;

	// method to get the logger (instance variable)
	public function getLogger(): Logger {

		return $this->logger;

	}

	// method to set the logger (instance variable)
	public function setLogger(Logger $logger) {

		$this->logger = $logger;

	}
}

$app = new Application;

// this line creates an anonymous class :D
$app->setLogger(new class implements Logger {

	// defining the log function ;)
	public function log(string $msg) {
		print($msg);
	}

});

$app->getLogger()->log("My First Log Message Using an Anonymous 
						Class that implements an Interface :D");

?>