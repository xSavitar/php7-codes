<?php
// Return type "int" is declared in functions.

declare(strict_types=1); // enforces the strict type mode

// this method returns integers only
function returnIntValue(int $value): int {
	return $value;
}

print(returnIntValue(5)); // return 5 as an int.

?>