<?php

// Strict mode
declare(strict_types=1);

function sum(int ...$ints) {

	return array_sum($ints);

}

// Relative to the previous mode, the call to 
// sum() will fail since a strict mode has been 
// enforced and sum must take only integers :D
print(sum(2, '3', 4.1));

?>